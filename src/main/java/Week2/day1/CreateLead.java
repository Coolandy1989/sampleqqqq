package Week2.day1;


import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testleaf.leaftaps.seleniumbase.ProjectSpecificMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateLead extends ProjectSpecificMethod {
	
	@BeforeClass
	public void setdata() {
		excelfilename = "Createdata";
	}
	
	
    @Test(dataProvider="inputdata")
	public  void main(String cname , String fname, String lname) throws InterruptedException  {
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();			
		driver.get("http://leaftaps.com/opentaps");		
		driver.manage().window().maximize();		
		driver.findElementById("username").sendKeys("DemoSalesManager");		
		driver.findElementById("password").sendKeys("crmsfa");		
		driver.findElementByClassName("decorativeSubmit").click();		
		driver.findElementByLinkText("CRM/SFA").click();
		
    	
    	
		driver.findElementByLinkText("Create Lead").click();
		
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
		
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
		driver.findElementByXPath("//input[@value='Create Lead']").click();
		
		
	    String str	= driver.findElementByXPath("//span[@id='viewLead_companyName_sp']").getText();
	    str = str.replaceAll("[^0-9]", "");
	    System.out.println(str);
	    
	    driver.findElementByXPath("//div/a[@class='subMenuButtonDangerous']").click();
	    driver.findElementByXPath("//a[text()='Find Leads']").click();
	    driver.findElementByXPath("//input[@name ='id']").sendKeys(str);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		String str1 = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		
		System.out.println(str1);
				
	}
    /*@DataProvider(name="inputdata")
    public  String[][] datainput(){
    	
    	String[][] data = new String[2][3];
    	
    	data[0][0] = "Infosys";
    	data[0][1] = "Dinesh";
    	data[0][2] = "Anderson";
    	
    	data[1][0] = "Infosys";
    	data[1][1] = "MS";
    	data[1][2] = "Dhoni";
    	
   	
		return data;
    	
    }
    */


	private void ReadExcel() {
		// TODO Auto-generated method stub
		
	}		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		
    		

}
