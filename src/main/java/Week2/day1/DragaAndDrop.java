package Week2.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragaAndDrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

        System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/drag.html");
		driver.manage().window().maximize();
		
		
		//driver.findElementByXPath("//h5[text()='Draggable']").click();
		WebElement source = driver.findElementById("draggable");
		Actions builder = new Actions(driver);
		builder.dragAndDropBy(source, 100, 100).perform();
		//driver.close();
		
		
		 /*WebElement ele1= driver.findElementById("createLeadForm_marketingCampaignId");
		 Select sel1 = new Select (ele1);
			
		
		List<WebElement> webele = sel1.getOptions();
        int size = webele.size();
        
        String text = sel1.getOptions().get(size-2).getText();*/
		
		driver.get("http://leafground.com/pages/selectable.html");
		driver.manage().window().maximize();
		
		Actions builder1 = new Actions(driver);
		WebElement ele1 = driver.findElementByXPath("//li[text()= 'Item 1']");
		WebElement ele2 = driver.findElementByXPath("//li[text()= 'Item 2']");
		WebElement ele3 = driver.findElementByXPath("//li[text()= 'Item 3']");
		
        
		builder1.clickAndHold(ele1).clickAndHold(ele2).clickAndHold(ele3).release(ele1).release(ele2).release(ele3).perform();
		builder.release().perform();
		
		
	}

}
