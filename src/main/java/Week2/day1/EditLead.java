package Week2.day1;


import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testleaf.leaftaps.seleniumbase.ProjectSpecificMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EditLead extends ProjectSpecificMethod {
	@BeforeClass
	public void setdata() {
		excelfilename = "EditLead";
	}
	
    @Test(dataProvider="inputdata")
	public  void edit(String fname1,String cname ,String fname) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
        
		driver.findElementByLinkText("Leads").click();
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		driver.findElementById("createLeadForm_firstName").sendKeys(fname1);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		driver.findElementByXPath("((//table[@class='x-grid3-row-table'])[1]//a)[1]").click();
		driver.findElementByLinkText("Edit").click();
		
		driver.findElementById("createLeadForm_companyName").clear();
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
		driver.findElementById("createLeadForm_firstName").clear();
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
		driver.findElementByXPath("//input[@value='Update']").click();
		String companyname = driver.findElementByXPath("//span[text()='Company Name']/following::span[1]").getText();
		String firstname = driver.findElementByXPath("//span[text()='First name']/following::span[1]").getText();
		File src = driver.getScreenshotAs(OutputType.FILE);
		File target = new File("./snaps/Img.jpg");
		FileUtils.copyFile(src, target);
		
		System.out.println("The update details "+companyname+" & "+firstname+".");
		driver.close();
			
	}

}
