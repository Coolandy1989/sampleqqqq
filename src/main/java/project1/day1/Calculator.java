package project1.day1;

public class Calculator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Calculator obj = new Calculator ();
		int a= 10,b=5;
		int add=obj.add(a, b);
		int sub=obj.Sub(a, b);
		int mul=obj.Mul(a, b);
		int div=obj.Div(a, b);
		System.out.println("Values displaying from Main method");
		System.out.println("===================================");
		System.out.println("The Sum of two numbers are "+ add); 
		System.out.println("The Difference of two numbers are "+ sub);
		System.out.println("The Multiplication of two numbers are "+ mul);
		System.out.println("The Division of two numbers are "+ div);
		System.out.println("-------------------------------------------------");
		obj.calc();

	}
	
	public int add(int a, int b) {
		int c=a+b;
		return c;
	}

	public int Sub(int a, int b) {
		int c=a-b;
		return c;
	}
	public int Mul(int a, int b) {
		int c=a*b;
		return c;
	}
	public int Div(int a, int b) {
		int c=a/b;
		return c;
	}
	public void calc() {
		int a= 20,b=5;
		Calculator obj1 = new Calculator();
		int add=obj1.add(a, b);
		int sub=obj1.Sub(a, b);
		int mul=obj1.Mul(a, b);
		int div=obj1.Div(a, b);
		System.out.println("Values displaying from Single method");
		System.out.println("=====================================");
		System.out.println("The Sum of two numbers are "+ add); 
		System.out.println("The Difference of two numbers are "+ sub);
		System.out.println("The Multiplication of two numbers are "+ mul);
		System.out.println("The Division of two numbers are "+ div);

	}

	
}
