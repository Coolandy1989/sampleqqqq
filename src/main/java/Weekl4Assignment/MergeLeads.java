package Weekl4Assignment;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLeads {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		
		driver.manage().window().maximize();
		
        driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");		
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		
		String parentwindow = driver.getWindowHandle();
		Thread.sleep(5000);
		
		Set<String> winhandles = driver.getWindowHandles();
		
	    for (String handles : winhandles) {
			
	    	if(!handles.equals(parentwindow)) {
	    		driver.switchTo().window(handles);
	    		Thread.sleep(1000);
	    		System.out.println("The Current window"+ driver.getTitle() );
	    		driver.findElementByXPath("//label[text()='Lead ID:']/following::input[1]").sendKeys("10035");
	    		driver.findElementByXPath("//button[text()='Find Leads']").click();
	    		//driver.findElementByLinkText("10035");
	    		driver.findElementByXPath("//a[text()='10035']").click();
//	    		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
	    	    driver.close();
	    	    
	    	    driver.switchTo().window(parentwindow);
	    	    driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
	    	    driver.switchTo().window(handles);
	    	    System.out.println("The Current window"+ driver.getTitle() );
	    		driver.findElementByXPath("//label[text()='Lead ID:']/following::input[1]").sendKeys("10037");
	    		driver.findElementByXPath("//button[text()='Find Leads']").click();
	    		//driver.findElementByLinkText("10037");
	    		driver.findElementByXPath("//a[text()='10037']").click();
//	    		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
	    	    driver.close();

	    	    
	    	}
			
		}
				
		driver.switchTo().window(parentwindow);
		/*driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
        for (String handles : winhandles) {
			
	    	if(!handles.equals(parentwindow)) {
	    		driver.switchTo().window(handles);
	    		Thread.sleep(1000);
	    		System.out.println("The Current window"+ driver.getTitle() );
	    		driver.findElementByXPath("//label[text()='Lead ID:']/following::input[1]").sendKeys("10037");
//	    		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
	    	    driver.close();
	    	}
		
		}*/
		

		
}	
		
		

	}



