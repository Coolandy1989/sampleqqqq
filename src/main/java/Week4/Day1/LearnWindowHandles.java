package Week4.Day1;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindowHandles {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
       System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		
		driver.findElementByLinkText("Contact Us");
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File target = new File ("./Snapshots/");
		FileUtils.copyFile(src, target);
		

	}

}
