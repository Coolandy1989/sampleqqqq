package Week4.Day1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class frames {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
       System.setProperty("webdriver.chrome.driver", "./drivers/Chromedriver/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		
		// Through Name
		//driver.switchTo().frame("iframeResult");
		
		// Through WebElement 
		WebElement frame = driver.findElementByXPath("// div[@id = \"iframewrapper\"]/iframe");
		
		driver.switchTo().frame(frame);
		
	    WebElement ele	= driver.findElementByXPath("//button[text()='Try it']");
		ele.click();
		//Switch to alert
		Alert alert = driver.switchTo().alert();
		System.out.println(alert.getText());
		alert.sendKeys("MS Dhoni");
		alert.accept();
		

	}

}
